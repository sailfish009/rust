#! /bin/bash

cp -a ../rust/C* .
cp -a ../rust/c* .
cp -a ../rust/.gitattributes .
cp -a ../rust/.gitmodules .
cp -a ../rust/L* .
cp -a ../rust/l* .
cp -a ../rust/.mailmap .
cp -a ../rust/R* .
cp -a ../rust/r* .
cp -a ../rust/src .
cp -a ../rust/triagebot.toml .
cp -a ../rust/x.py .
